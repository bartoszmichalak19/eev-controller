/*
 * sensors.c
 *
 *  Created on: 06.09.2018
 *      Author: michalak
 */


#include "sensors.h"



void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
	for (int i =0; i<5; i++)
	{
	   adcMsr[i] = buf[i];  // store the values in adc[]
	}

        eoc = 1;
}

void average(){


	if(eoc){ //sprawdza czy zakonczyla sie konwersja
	if(!avg_done){
		//pamietac zeby samplesTemp>samplesVolt
		if(actual_samples < samplesTemp){
			T1.sum += adcMsr[0];
			T2.sum += adcMsr[1];
			T3.sum += adcMsr[2];
				if(actual_samples < samplesVolt){
				P1.sum += adcMsr[3];
				POT.sum += adcMsr[4];
				}
			actual_samples++;
		}
		else{

			T1.result_adc = T1.sum/samplesTemp;
			T2.result_adc = T2.sum/samplesTemp;
			T3.result_adc = T3.sum/samplesTemp;
			P1.result_adc = P1.sum/samplesVolt;
			POT.result_adc = POT.sum/samplesVolt;

			eoc = 0;
			avg_done = 1; // skonczone filtrowanie, mozna przeliczyc

		}
	}
	}

}


void calculate_value(){
	if(avg_done){
		//je�li uda si� dobrac Rezystor przed NTC to mozna usunac pierwszy logarytm bo sie wyzeruje
		T1.value = (beta/(log(Rref/Rref) + log((adc_resolution-T1.result_adc)/T1.result_adc) + beta/Tref)) - 273.15;
		T2.value = (beta/(log(Rref/Rref) + log((adc_resolution-T2.result_adc)/T2.result_adc) + beta/Tref)) - 273.15;
		T3.value = (beta/(log(Rref/Rref) + log((adc_resolution-T3.result_adc)/T3.result_adc) + beta/Tref)) - 273.15;

		POT.value = POT.result_adc*supply_voltage/adc_resolution*((47+68)/68);
		P1.value = P1.result_adc*supply_voltage/adc_resolution*((47+68)/68); //ostatni skladnik to stosunek rezystor�w dzielnika

		avg_done = 0; //ustawia flage zeby mozna bylo zaczac pomiar z adc
		actual_samples = 0;
		T1.sum = 0;
		T2.sum = 0;
		T3.sum = 0;
		P1.sum = 0;
		POT.sum = 0;
		mdv.actual_deltaT = T2.value - T1.value;
	}
}


