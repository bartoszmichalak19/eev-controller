/*
 * stepper.c
 *
 *  Created on: 16.08.2018
 *      Author: michalak
 */

#include "stepper.h"

stepper valve;

void rotate(int steps){

	//jesli kroki ujemne to odkrecaj, jesli dodatnie zakrecaj
	if(!valve.rotateInit){

		valve.rotateInit = 1;
		valve.savedStep = valve.actualStep;

			if(steps>0) set_direction(1);
			else set_direction(-1);
//			printf("rot1\r\n");
//						osDelay(500);
	}

//				if(steps>0) set_direction(1);
//				else set_direction(-1);

	while(!(valve.actualStep == (valve.savedStep+steps))){
		take_step();
		mdv.actual_opening  = (valve.actualStep/valve.totalSteps)*100.0;
	}

	enable(0);

//	if(valve.actualStep <= valve.savedStep){
//		take_step();
//	}
//	else{
//
//	}
}

void take_step(){


	//tu ma się robic krok co okreslony czas wyliczany z zadanej czestotliwosci
	HAL_GPIO_WritePin(STEP_GPIO_Port,STEP_Pin,1);
	osDelay(1000/valve.frequency-1);
	//	osDelay(1000/valve.frequency-10);
	HAL_GPIO_WritePin(STEP_GPIO_Port,STEP_Pin,0);
	osDelay(1); 									//Toff
	valve.actualStep += valve.direction; //aktualizuje aktualny krok w zaleznosci od kierunku obrotu
//	mdv.actual_opening  = (valve.actualStep/valve.totalSteps)*100.0;
	//	printf("step \r\n");



}


void enable(uint8_t on){
	if(on){
		HAL_GPIO_WritePin(EN_STEP_GPIO_Port,EN_STEP_Pin,0); //a4988 low = working stepper
		valve.enable = 1;
	}
	else{
		HAL_GPIO_WritePin(EN_STEP_GPIO_Port,EN_STEP_Pin,1);
		valve.enable = 0;
		valve.rotateInit = 0;
	}

	printf("enable\r\n");
//		osDelay(500);
}

void set_direction(int8_t dir){
	//zakrecanie
	if(dir==1){
		HAL_GPIO_WritePin(DIR_GPIO_Port,DIR_Pin,1);
			valve.direction = 1;
		}
	//odkrecanie
		if(dir==-1){
			HAL_GPIO_WritePin(DIR_GPIO_Port,DIR_Pin,0);
			valve.direction = -1;
		}

		printf("dir\r\n");
//			osDelay(500);
}


void init_stepstick(){ //initial values of stepstick structure

	    valve.actualStep = 0;         //actual step of motor
		valve.savedStep = 0;			 //needed to save previous step
		valve.totalSteps = 256;		  //total steps to 100% open valve
//		valve.initialStep;		  //starting point after initial function
		valve.frequency = 25;			  //steps per second
		valve.direction = 1;			  //direction of rotation 1-opening -1-closing
		valve.enable = 0;				  //turn's on/off valve
		valve.rotateInit = 0;			  //info whether valve was based before rotate
		valve.setStep = mdv.initial_value/100*valve.totalSteps;			  //actual setStep

}
