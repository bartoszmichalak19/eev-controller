/*
 * GUI.c
 *
 *  Created on: 13.11.2018
 *      Author: DELL
 */

#include "GUI.h"



uint8_t menuUse;         		//if button is used in menu =1
uint8_t flag_set;
uint8_t oled_refresh = 0;
float tmp2 = 0;
float tmp3[4] = {0,0,0,0};
char buffer[16];

void button_init(Button *sharp) {
	sharp->isOn = 0; // czy wcisniety
	sharp->onConfidenceLVL = 0;
	sharp->offConfidenceLVL = 0;
	sharp->confidenceTreshold = 100; //prog przelaczenia
	sharp->eventRising = 0;
	menuUse = 1;
	flag_set = 0;
}

void settings() {

	int tmp[2];
	switch (flag_set) {
	case 1:
		if (tmp2 != mdv.set_deltaT) { //wyswietlaj tylko jesli zmieni sie ustawiony parametr

			if (!tmp2) { //jesli wykonuje sie pierwszy raz
				ssd1331_clear_screen(BLACK);
				ssd1331_display_string(0, 0, "   DeltaT", FONT_1608, GOLDEN);

			}

			tmp[0] = mdv.set_deltaT * 10;
			tmp[1] = tmp[0] % 10;
			tmp[0] = mdv.set_deltaT;

			//TODO napisac obsluge znakow ujemnych w wyswietlaniu
//		    		if(tmp[1]<0){
//		    		sprintf(buffer, "%d.%d", tmp[0],tmp[1]*-1);
//		    		}

			sprintf(buffer, "  %d.%d [*C]", tmp[0], tmp[1]);
			ssd1331_clear_screen1608(32, BLACK);
			ssd1331_display_string(0, 32, buffer, FONT_1608, WHITE);
			tmp2 = mdv.set_deltaT;
		}

		if (inc.eventRising)
			mdv.set_deltaT += 0.1;
		if (dec.eventRising)
			mdv.set_deltaT -= 0.1;
		if (enter.eventRising) {
			menuUse = 1;				//wlacz przyciski do obslugi menu
			flag_set = 0;				//wylacz obsluge nastaw
			tmp2 = 0;						//
			mk_menu_toggle();
		}

		break;

	case 2:
		if (tmp2 != mdv.initial_value) { //wyswietlaj tylko jesli zmieni sie ustawiony parametr

			if (!tmp2) { //jesli wykonuje sie pierwszy raz
				ssd1331_clear_screen(BLACK);
				ssd1331_display_string(0, 0, "  OTWARCIE", FONT_1608, GOLDEN);
				ssd1331_display_string(0, 16, "   ZEROWE", FONT_1608, GOLDEN);
			}

			//TODO napisac obsluge znakow ujemnych w wyswietlaniu
			//		    		if(tmp[1]<0){
			//		    		sprintf(buffer, "%d.%d", tmp[0],tmp[1]*-1);
			//		    		}

			sprintf(buffer, "    %d", mdv.initial_value);
			ssd1331_clear_screen1608(32, BLACK);
			ssd1331_display_string(0, 32, buffer, FONT_1608, WHITE);
			ssd1331_display_string(32, 32, "  [%]", FONT_1608, WHITE);
			tmp2 = mdv.initial_value;
		}

		if (inc.eventRising)
			++mdv.initial_value;
		if (dec.eventRising)
			--mdv.initial_value;
		if (enter.eventRising) {
			menuUse = 1;				//wlacz przyciski do obslugi menu
			flag_set = 0;				//wylacz obsluge nastaw
			tmp2 = 0;						//zeruj wartosc tymczasową
			mk_menu_toggle();
		}

		break;

	case 3:
		if (tmp2 != PID.Kp) { //wyswietlaj tylko jesli zmieni sie ustawiony parametr

			if (!tmp2) { //jesli wykonuje sie pierwszy raz
				ssd1331_clear_screen(BLACK);
				ssd1331_display_string(0, 0, "     Kp", FONT_1608, GOLDEN);
			}
			tmp[0] = PID.Kp * 100;
			tmp[1] = tmp[0] % 100;
			tmp[0] = PID.Kp;

			//TODO napisac obsluge znakow ujemnych w wyswietlaniu
			//		    		if(tmp[1]<0){
			//		    		sprintf(buffer, "%d.%d", tmp[0],tmp[1]*-1);
			//		    		}

			sprintf(buffer, "    %d.%d ", tmp[0], tmp[1]);
			ssd1331_clear_screen1608(32, BLACK);
			ssd1331_display_string(0, 32, buffer, FONT_1608, WHITE);
			tmp2 = PID.Kp;
		}

		if (inc.eventRising)
			PID.Kp += 0.01;
		if (dec.eventRising)
			PID.Kp -= 0.01;
		if (enter.eventRising) {
			menuUse = 1;				//wlacz przyciski do obslugi menu
			flag_set = 0;				//wylacz obsluge nastaw
			tmp2 = 0;						//
			mk_menu_toggle();
		}

		break;

	case 4:
		if (tmp2 != PID.Ki) { //wyswietlaj tylko jesli zmieni sie ustawiony parametr

			if (!tmp2) { //jesli wykonuje sie pierwszy raz
				ssd1331_clear_screen(BLACK);
				ssd1331_display_string(0, 0, "     Ki", FONT_1608, GOLDEN);
			}
			tmp[0] = PID.Ki * 100;
			tmp[1] = tmp[0] % 100;
			tmp[0] = PID.Ki;

			//TODO napisac obsluge znakow ujemnych w wyswietlaniu
			//		    		if(tmp[1]<0){
			//		    		sprintf(buffer, "%d.%d", tmp[0],tmp[1]*-1);
			//		    		}

			sprintf(buffer, "    %d.%d", tmp[0], tmp[1]);
			ssd1331_clear_screen1608(32, BLACK);
			ssd1331_display_string(0, 32, buffer, FONT_1608, WHITE);
			tmp2 = PID.Ki;
		}

		if (inc.eventRising)
			PID.Ki += 0.01;
		if (dec.eventRising)
			PID.Ki -= 0.01;
		if (enter.eventRising) {
			menuUse = 1;				//wlacz przyciski do obslugi menu
			flag_set = 0;				//wylacz obsluge nastaw
			tmp2 = 0;						//
			mk_menu_toggle();
		}

		break;
	case 5:
		if (tmp2 != PID.Kd) { //wyswietlaj tylko jesli zmieni sie ustawiony parametr

			if (!tmp2) { //jesli wykonuje sie pierwszy raz
				ssd1331_clear_screen(BLACK);
				ssd1331_display_string(0, 0, "     Kd", FONT_1608, GOLDEN);
			}
			tmp[0] = PID.Kd * 100;
			tmp[1] = tmp[0] % 100;
			tmp[0] = PID.Kd;

			//TODO napisac obsluge znakow ujemnych w wyswietlaniu
			//		    		if(tmp[1]<0){
			//		    		sprintf(buffer, "%d.%d", tmp[0],tmp[1]*-1);
			//		    		}

			sprintf(buffer, "    %d.%d", tmp[0], tmp[1]);
			ssd1331_clear_screen1608(32, BLACK);
			ssd1331_display_string(0, 32, buffer, FONT_1608, WHITE);
			tmp2 = PID.Kd;
		}

		if (inc.eventRising)
			PID.Kd += 0.01;
		if (dec.eventRising)
			PID.Kd -= 0.01;
		if (enter.eventRising) {
			menuUse = 1;				//wlacz przyciski do obslugi menu
			flag_set = 0;				//wylacz obsluge nastaw
			tmp2 = 0;						//
			mk_menu_toggle();
		}

		break;

	case 6:	//PARAMETRY

		if(oled_refresh){

			if (!tmp2) { //jesli wykonuje sie pierwszy raz
							ssd1331_clear_screen(BLACK);
							ssd1331_display_string(0, 0, "  PARAMETRY", FONT_1608, GOLDEN);
							tmp2=1.0;
						}

			tmp[0] = mdv.actual_opening;

			//TODO napisac obsluge znakow ujemnych w wyswietlaniu
			//		    		if(tmp[1]<0){
			//		    		sprintf(buffer, "%d.%d", tmp[0],tmp[1]*-1);
			//		    		}

			int n = sprintf(buffer, "OTWARCIE=%d", tmp[0]);
			ssd1331_clear_screen1206(16, BLACK);
//			ssd1331_clear_screen1206xy(9 * 6, 16, BLACK);
			ssd1331_display_string(0, 16, buffer, FONT_1206, WHITE);
			ssd1331_display_string(n * 6, 16, "[%]", FONT_1206, WHITE);
			tmp3[0] = mdv.actual_opening;




			tmp[0] = mdv.actual_deltaT * 10;
			tmp[1] = tmp[0] % 10;
			tmp[0] = mdv.actual_deltaT;

			//TODO napisac obsluge znakow ujemnych w wyswietlaniu
			//		    		if(tmp[1]<0){
			//		    		sprintf(buffer, "%d.%d", tmp[0],tmp[1]*-1);
			//		    		}

			sprintf(buffer, "DeltaT=%d.%d[*C]", tmp[0], tmp[1]);
			ssd1331_clear_screen1206(28, BLACK);
			ssd1331_display_string(0, 28, buffer, FONT_1206, WHITE);




			tmp[0] = T1.value * 10;
			tmp[1] = tmp[0] % 10;
			tmp[0] = T1.value;

			//TODO napisac obsluge znakow ujemnych w wyswietlaniu
			//		    		if(tmp[1]<0){
			//		    		sprintf(buffer, "%d.%d", tmp[0],tmp[1]*-1);
			//		    		}

			sprintf(buffer, "T1=%d.%d [*C]", tmp[0], tmp[1]);
			ssd1331_clear_screen1206(40, BLACK);
			ssd1331_display_string(0, 40, buffer, FONT_1206, WHITE);



			tmp[0] = T2.value * 10;
			tmp[1] = tmp[0] % 10;
			tmp[0] = T2.value;

			//TODO napisac obsluge znakow ujemnych w wyswietlaniu
			//		    		if(tmp[1]<0){
			//		    		sprintf(buffer, "%d.%d", tmp[0],tmp[1]*-1);
			//	    		}
			sprintf(buffer, "T2=%d.%d [*C]", tmp[0], tmp[1]);
			ssd1331_clear_screen1206(52, BLACK);
			ssd1331_display_string(0, 52, buffer, FONT_1206, WHITE);


		oled_refresh = 0;
		}


		if (enter.eventRising) {
			menuUse = 1;				//wlacz przyciski do obslugi menu
			flag_set = 0;				//wylacz obsluge nastaw
			mk_menu_toggle();
		}

		break;
	};

}

void check_events() {
	if (enter.isOn && enter.eventRising) {
		if (menuUse)
			mk_menu_click();
		enter.eventRising = 0;
	}

	if (inc.isOn && inc.eventRising) {
		if (menuUse)
			mk_menu_inc();
		inc.eventRising = 0;
	}

	if (dec.isOn && dec.eventRising) {
		if (menuUse)
			mk_menu_dec();
		dec.eventRising = 0;
	}

	if (back.isOn && back.eventRising) {
		mk_menu_toggle();
		back.eventRising = 0;
	}

}

void button_check_state(Button *sharp, GPIO_PinState state) {
	if (state) {
		if (sharp->isOn == 0) {
			if (sharp->onConfidenceLVL > sharp->confidenceTreshold) {
				sharp->isOn = 1;
				sharp->eventRising = 1;
			} else {
				sharp->onConfidenceLVL++;
				sharp->offConfidenceLVL = 0;
			}
		}
	} else {
		if (sharp->isOn == 1) {
			if (sharp->offConfidenceLVL > sharp->confidenceTreshold) {
				sharp->isOn = 0;

			} else {
				sharp->offConfidenceLVL++;
				sharp->onConfidenceLVL = 0;
			}
		}
	}
}
