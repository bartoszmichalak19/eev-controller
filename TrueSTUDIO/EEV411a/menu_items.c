/*
 * mk_items.c
 *
 *  Created on: 13.11.2018 22:52:54
 *      Author: Miros�aw Karda�
 *         web: www.atnel.pl
 *   generator: MkMenuGen.EXE
 *   Copyright: ATNEL Miros�aw Karda�
 *   Nieautoryzowane kopiowanie, sprzeda�, udost�pnianie
 *   kodu �r�owego bez zgody autora jest zabronione.
 *   Naruszeniem praw autorskich jest r�wnie� publikowanie
 *   kodu �r�d�owego w internecie na forach, blogach itp.
 *
 */
//#include <avr/io.h>
//#include <pgmspace.h>
#include <stdio.h>

#include "mk_menu.h"
#include "menu_items.h"

#define PROGMEM


const char main_menu_string[] PROGMEM = {"EEV MENU"};

uint8_t nodes_count;
TNODE * nodes_list[ MAX_LEVEL_NODES ];
int8_t nodes_cursor;
int8_t screen_cursor;

const TNODE * const first_node_ptr PROGMEM = &NGSM0Fa_0;

//---------------- MENU ---------------

const char str0_NGSM0Fa[] PROGMEM = "NASTAWY";

    const char str1_LQAP10a[] PROGMEM = "DELTA T";
    const char str2_SOAT11a[] PROGMEM = "PKT ZERO";
    const char str3_JGIE12a[] PROGMEM = "REGULATOR";

        const char str4_KFSJ13a[] PROGMEM = "KP";
        const char str5_IKMJ14a[] PROGMEM = "KI";
        const char str6_GGLB15a[] PROGMEM = "KD";
        const char str7_back[] PROGMEM = "BACK";


const char str9_MURN18a[] PROGMEM = "PARAMETRY";
const char str10_exit[] PROGMEM = "EXIT";



const TNODE NGSM0Fa_0 PROGMEM = {
        0,
        str0_NGSM0Fa,
        -1,
        -1,
        &LQAP10a_1,
        NULL,
        &MURN18a_9,
        NULL,
};


const TNODE LQAP10a_1 PROGMEM = {
        1,
        str1_LQAP10a,
        0,
        -1,
        NULL,
        &NGSM0Fa_0,
        &SOAT11a_2,
        NULL,
};


const TNODE SOAT11a_2 PROGMEM = {
        2,
        str2_SOAT11a,
        1,
        -1,
        NULL,
        &NGSM0Fa_0,
        &JGIE12a_3,
        &LQAP10a_1,
};


const TNODE JGIE12a_3 PROGMEM = {
        3,
        str3_JGIE12a,
        -1,
        -1,
        &KFSJ13a_4,
        &NGSM0Fa_0,
        &back_8,
        &SOAT11a_2,
};


const TNODE KFSJ13a_4 PROGMEM = {
        4,
        str4_KFSJ13a,
        2,
        -1,
        NULL,
        &JGIE12a_3,
        &IKMJ14a_5,
        NULL,
};


const TNODE IKMJ14a_5 PROGMEM = {
        5,
        str5_IKMJ14a,
        3,
        -1,
        NULL,
        &JGIE12a_3,
        &GGLB15a_6,
        &KFSJ13a_4,
};


const TNODE GGLB15a_6 PROGMEM = {
        6,
        str6_GGLB15a,
        4,
        -1,
        NULL,
        &JGIE12a_3,
        &back_7,
        &IKMJ14a_5,
};


const TNODE back_7 PROGMEM = {
        7,
        str7_back,
        -1,
        -1,
        NULL,
        &JGIE12a_3,
        NULL,
        &GGLB15a_6,
};


const TNODE back_8 PROGMEM = {
        8,
        str7_back,
        -1,
        -1,
        NULL,
        &NGSM0Fa_0,
        NULL,
        &JGIE12a_3,
};


const TNODE MURN18a_9 PROGMEM = {
        9,
        str9_MURN18a,
        5,
        -1,
        NULL,
        NULL,
        &exit_10,
        &NGSM0Fa_0,
};


const TNODE exit_10 PROGMEM = {
        10,
        str10_exit,
        -1,
        -1,
        NULL,
        NULL,
        NULL,
        &MURN18a_9,
};
