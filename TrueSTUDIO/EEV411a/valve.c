/*
 * valve.c
 *
 *  Created on: 31.08.2018
 *      Author: michalak
 */


#include "valve.h"



void base_valve(){
	enable(1);
//	uint8_t initValueFlag = 0;
	if(!mdv.valveInit){

		if(valve.enable){
			rotate(-1.5*valve.totalSteps);
			valve.actualStep = 0;
			mdv.actual_opening = 0;
			enable(1);


				rotate(mdv.initial_value*valve.totalSteps/100);
				if(!valve.enable){
//					initValueFlag = 1;
					mdv.valveInit = 1;

			}
		}
	}
}

void control_alghoritm(){

	if(mdv.PID_loop && mdv.valveInit){
		PID.error = mdv.set_deltaT - mdv.actual_deltaT;

		PID.prop = PID.Kp * PID.error;
		PID.inte = PID.Ki * (PID.error+PID.prev_error)/2;  //calkowanie metoda trapezow
		PID.deri = PID.Kd * (PID.error-PID.prev_error);


		PID.prev_error = PID.error;
		mdv.PID_loop = 0;

		mdv.set_opening -= (PID.prop + PID.inte + PID.deri);  //gdy delta za duza to otwieraj zawor
		if(mdv.set_opening >=100)mdv.set_opening = 100;
		if(mdv.set_opening <= 0)mdv.set_opening = 0;
	}

}


void maintain_opening(){

	if(mdv.actual_opening!=mdv.set_opening && !valve.enable){
//calculate how many steps valve needs to reach set_opening
		valve.setStep = (mdv.set_opening - mdv.actual_opening) * valve.totalSteps/100;
		if(valve.setStep + valve.actualStep >= valve.totalSteps )
			valve.setStep = valve.setStep + valve.actualStep - valve.totalSteps; //wtedy otworz max
		if(valve.setStep + valve.actualStep <= 0)
			valve.setStep = -valve.actualStep; //wtedy zamknij max
//then set valve enable and task could rotate the motor
		enable(1);
	}



}


void init_valve(){
		mdv.initial_value = 60;		//initial open of EEV [0-100%]
		mdv.actual_opening = 0;		//actual open of EEV [0-100%]
		mdv.set_opening = mdv.initial_value;		//set opening of EEV [0-100%]
		mdv.eev_enable = 1;			//turn's on/off valve
		mdv.manual = 0;				//valve is in auto mode or manual
		mdv.valveInit = 0;			////info whether valve was based before rotate
		mdv.actual_deltaT = 0.0;	//
		mdv.set_deltaT = 1.0;		//set superheat
		mdv.PID_loop = 0;			//info about state of PID_loop


		//PID parameters
		PID.error = PID.prev_error = PID.prop = PID.inte = PID.deri = 0.0;
		PID.Kp = 1.2;
		PID.Ki = 0.01;
		PID.Kd = 0.3;
}
