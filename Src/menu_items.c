/*
 * mk_items.c
 *
 *  Created on: 13.11.2018 22:52:54
 *      Author: Miros�aw Karda�
 *         web: www.atnel.pl
 *   generator: MkMenuGen.EXE
 *   Copyright: ATNEL Miros�aw Karda�
 *   Nieautoryzowane kopiowanie, sprzeda�, udost�pnianie
 *   kodu �r�owego bez zgody autora jest zabronione.
 *   Naruszeniem praw autorskich jest r�wnie� publikowanie
 *   kodu �r�d�owego w internecie na forach, blogach itp.
 *
 */
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <stdio.h>

#include "mk_menu.h"
#include "menu_items.h"


const char main_menu_string[] PROGMEM = {"EEV MENU"};

uint8_t nodes_count;
TNODE * nodes_list[ MAX_LEVEL_NODES ];
int8_t nodes_cursor;
int8_t screen_cursor;

const TNODE * const first_node_ptr PROGMEM = &NQGS0Fa_0;

//---------------- MENU ---------------

const char str0_NQGS0Fa[] PROGMEM = "NASTAWY";

    const char str1_CRAB10a[] PROGMEM = "DELTA T";
    const char str2_JPVH11a[] PROGMEM = "POZYCJA ZERO";
    const char str3_MLHU12a[] PROGMEM = "CZESTOTLIWOSC";
    const char str4_IEIL13a[] PROGMEM = "ILOSC KROKOW";
    const char str5_back[] PROGMEM = "BACK";

const char str6_DPRQ15a[] PROGMEM = "PARAMETRY";


const char str8_exit[] PROGMEM = "EXIT";



const TNODE NQGS0Fa_0 PROGMEM = {
        0,
        str0_NQGS0Fa,
        -1,
        -1,
        &CRAB10a_1,
        NULL,
        &DPRQ15a_6,
        NULL,
};


const TNODE CRAB10a_1 PROGMEM = {
        1,
        str1_CRAB10a,
        0,
        0,
        NULL,
        &NQGS0Fa_0,
        &JPVH11a_2,
        NULL,
};


const TNODE JPVH11a_2 PROGMEM = {
        2,
        str2_JPVH11a,
        1,
        1,
        NULL,
        &NQGS0Fa_0,
        &MLHU12a_3,
        &CRAB10a_1,
};


const TNODE MLHU12a_3 PROGMEM = {
        3,
        str3_MLHU12a,
        2,
        2,
        NULL,
        &NQGS0Fa_0,
        &IEIL13a_4,
        &JPVH11a_2,
};


const TNODE IEIL13a_4 PROGMEM = {
        4,
        str4_IEIL13a,
        3,
        3,
        NULL,
        &NQGS0Fa_0,
        &back_5,
        &MLHU12a_3,
};


const TNODE back_5 PROGMEM = {
        5,
        str5_back,
        -1,
        -1,
        NULL,
        &NQGS0Fa_0,
        NULL,
        &IEIL13a_4,
};


const TNODE DPRQ15a_6 PROGMEM = {
        6,
        str6_DPRQ15a,
        -1,
        -1,
        &back_7,
        NULL,
        &exit_8,
        &NQGS0Fa_0,
};


const TNODE back_7 PROGMEM = {
        7,
        str5_back,
        -1,
        -1,
        NULL,
        &DPRQ15a_6,
        NULL,
        NULL,
};


const TNODE exit_8 PROGMEM = {
        8,
        str8_exit,
        -1,
        -1,
        NULL,
        NULL,
        NULL,
        &DPRQ15a_6,
};
