/*
 * mk_menu_display.c
 *
 *  Created on: 2018-01-30 
 *      Author: Miros�aw Karda�
 *         web: www.atnel.pl
 *   generator: MkMenuGen.EXE
 *   Copyright: ATNEL Miros�aw Karda�
 *   Nieautoryzowane kopiowanie, sprzeda�, udost�pnianie 
 *   kodu �r�owego bez zgody autora jest zabronione.
 *   Naruszeniem praw autorskich jest r�wnie� publikowanie 
 *   kodu �r�d�owego w internecie na forach, blogach itp.
 *
 */
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <string.h>
#include <stdio.h>

// #include " your display library "
// #include "../MK_OLED/mk_ssd1306.h"		// OLED
// #include "../LCD/lcd44780.h"			// simple LCD 2x16


#include "../MK_MENU_LIB/mk_menu.h"
#include "../MK_MENU_LIB/menu_items.h"


#include "mk_menu_display.h"




TPROPERTY menu_property[ MENU_PROPERTY_COUNT ] = {
/* 
TPROPERTY menu_property[ MENU_PROPERTY_COUNT ] = {
		{.type = _byte},									// SCK OUT Pin NR
		{.type = _bool},									// NTP SYNC
		{.type = _bool},									// PC SYNC
		{.type = _bool},									// DATE ON/OFF
		{.type = _bool},									// Modbus Enabled
		{.type = _int, .int16 = 0, .def_int16 = 0},			// Modbus Reg ADR
//----------------------- IP Properties (W�a�ciwo�ci typu IP) -------------------------
//---- przyk�ad obs�ugi tych w�a�ciwo�ci w ramach mk_menu_exe.c - PRZYCISK "OLED sample"
		{.type = _ip,
				.tab[3]=192, .tab[2]=168, .tab[1]=1, .tab[0]=10,
				.def_tab[3]=192, .def_tab[2]=168, .def_tab[1]=1, .def_tab[0]=10,
		},
		{.type = _ip,
				.tab[3]=255, .tab[2]=255, .tab[1]=255, .tab[0]=0,
				.def_tab[3]=255, .def_tab[2]=255, .def_tab[1]=255, .def_tab[0]=0,
		},
		{.type = _ip,
				.tab[3]=192, .tab[2]=168, .tab[1]=1, .tab[0]=1,
				.def_tab[3]=192, .def_tab[2]=168, .def_tab[1]=1, .def_tab[0]=1,
		},
//----------------------- IP Properties (W�a�ciwo�ci typu IP) -------------------------
		{.type = _str, .string = host_name },

		{.type = _bool},									// Buzzer Enabled
		{.type = _byte},									// Buzzer Pin Nr
		{.type = _bool},									// Buzzer AutoMode On/OFF
		{.type = _bool},									// Buzzer AutoMode NightMode
		{.type = _int, .int16 = 5, .def_int16 = 5},			// Volume Max (0-10)
		{.type = _byte, .byte = 1, .def_byte = 1},			// LCD Rows (1-4)
		{.type = _byte, .byte = 16, .def_byte = 16},			// LCD Columns (1-16)
		{.type = _bool},									// LCD Enabled
		{.type = _bool},									// UART Enabled
		{.type = _byte, .byte = 8, .def_byte = 8},			// UART Bits
};
*/		
};							



void on_display_cls( void ) {
	// function for clear screen for example
	
	// example for OLED 
	// mk_ssdD1306_cls();
	
	// example for simple LCD 2x16
	// lcd_cls();
}

void on_display_refresh( void ) {
	// refresh your screen if nedded
	// if you use simple LCD HD44780 this function can be empty
	
	// example for OLED 
	// mk_ssd1306_display();
	
	// example for simple LCD 2x16
	// not nedded
}


#if MULTILINE_MENU == 1

void on_display_menu_header( const char * astr ) {
	// function for display string from FLASH for example 
	// it's your decision which coordinates x,y will you use
	
	// example for OLED 
	// mk_ssd1306_puts_P( 0,0, astr, 2, 1, 0);
	
	// example for simple LCD 2x16
	// lcd_locate( 0, 0 );
	// lcd_str_P( astr );
}

void on_display_menu_item( int y, const char * item_string ) {
	// function for display string from FLASH for example 
	// it's your decision which coordinate x will you use
	// coordinate y is taken in first argument
	 
	// example for OLED 	 
	// mk_ssd1306_puts_P( (MAGNIFI*8)+2, y, item_string, MAGNIFI, 1, 0);
	
	// example for simple LCD 2x16
	// lcd_locate( y, 1 );
	// lcd_str_P( item_string );	
}

void on_display_menu_item_property( int y, uint8_t prop_idx ) {
	
	// your procedure to display property for desired Menu Item 
	// you become y coordinate and prop_idx (property index)
	// you decide about x coordinate
	// in this example we display property as right aligned
	
	// sample for display two kinds of property _bool and _int 
	/*
	char tbuf[10];
	tbuf[0] = 0;
    if( menu_property[ prop_idx ].type == _bool ) {
        if( menu_property[ prop_idx ].byte == menu_property[ prop_idx ].def_byte ) strcat_P( tbuf, PSTR("*(") );
        else strcat_P( tbuf, PSTR("(") );
        if( menu_property[ prop_idx ].byte ) strcat_P( tbuf, PSTR("ON)") );
        else strcat_P( tbuf, PSTR("OFF)") );
    }
    else if( menu_property[ prop_idx ].type == _byte ) {
        if( menu_property[ prop_idx ].byte == menu_property[ prop_idx ].def_byte ) {
            sprintf_P( tbuf, PSTR("*(%d)"), menu_property[ prop_idx ].byte );
        } else {
            sprintf_P( tbuf, PSTR("(%d)"), menu_property[ prop_idx ].byte );
        }
    }
    else if( menu_property[ prop_idx ].type == _int ) {
        if( menu_property[ prop_idx ].int16 == menu_property[ prop_idx ].def_int16 ) {
            sprintf_P( tbuf, PSTR("*(%d)"), menu_property[ prop_idx ].int16 );
        } else {
            sprintf_P( tbuf, PSTR("(%d)"), menu_property[ prop_idx ].int16 );
        }
        if( menu_property[ prop_idx ].int16 < 0 ) sprintf_P( tbuf, PSTR("(OFF)") );
    }
	uint8_t len = strlen( tbuf );
	*/
	
	// example for OLED 
	// mk_ssd1306_puts( 128-(len*6), y, tbuf, MAGNIFI, 1, 0);
	
	// example for simple LCD 2x16
	// lcd_locate( y, 16-len );
	// lcd_str( tbuf );
}

void on_display_item_cursor( int y ) {
	// function for display your cursor 
	// you become y coordinate and you decide about x coordinate
	
	// example for OLED 
	// mk_ssd1306_puts_P( 0, y, PSTR("\xFE"), MAGNIFI, 1, 0);
	
	// example for simple LCD 2x16
	// lcd_locate( y, 0 );
	// lcd_str_P( PSTR(">") );	
}

void on_clear_item_cursor( void ) {
	// function for clear your cursor
	// remember that y coordinate you have to get from *** get_y_pos(screen_cursor) *** as below
	
	// example for OLED 
	// mk_ssd1306_puts_P( 0, get_y_pos(screen_cursor), PSTR(" "), MAGNIFI, 1, 0);
	
	// example for simple LCD 2x16	
	// lcd_locate( get_y_pos(screen_cursor), 0 );
	// lcd_str_P( PSTR(" ") );	
}

#endif


void display_menu_node( const char * node_str ) {
	// it can be your callback called from MkMENU_LIB when 
	// there it's Menu Item change
	// you becom in argument string with Item string 


	// example for simple LCD 2x16
	//#if MULTILINE_MENU == 0
	//	lcd_locate(1,0);
	//	lcd_str_P( PSTR("                ") );
	//	lcd_locate(1,0);
	//	lcd_str_P( node_str );
	//#endif

}


void display_menu_branch( const char * branch_node_str, TNODE * active_node, TNODE * first_node ) {
	// it can be your callback called from MkMENU_LIB when 
	// there it's all Menu/Submenu change
	// arguments:
	// branch_node_str - name about Menu/Submenu Header
	// active_node - it's active node in working menu (linked with node_cursor in multiline mode menu)
	// first_node - it's allways pointer to first node in Menu/SubMenu
	

	// example for simple LCD 2x16	
	//#if MULTILINE_MENU == 0
	//	lcd_cls();
	//	if( first_node == get_first_node() ) lcd_str( "[ MENU ]" );
	//	else {
	//		lcd_str_P( PSTR("[ ") );
	//		lcd_str_P( branch_node_str );
	//		lcd_str_P( PSTR(" ]") );
	//	}
	//	lcd_locate(1,0);
	//	lcd_str_P( get_node_title(active_node) );
	//#endif	
	
	
}






