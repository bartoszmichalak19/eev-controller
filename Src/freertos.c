/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */     
#include "valve.h"
#include "stepper.h"
#include "sensors.h"
#include "GUI.h"
#include "SSD1331.h"
#include "mk_menu.h"

#include "mk_menu.h"
#include "menu_items.h"
#include "mk_menu_exe.h"
#include "mk_menu_display.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/* USER CODE END Variables */
osThreadId motorControlTasHandle;
osThreadId PIDvalveTaskHandle;
osThreadId GUITaskHandle;
osThreadId manageTaskHandle;
osThreadId tempTaskHandle;
osTimerId PIDtimerHandle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void StartMotorControlTask(void const * argument);
void StartPIDvalveTask(void const * argument);
void StartGUITask(void const * argument);
void StartManageTask(void const * argument);
void StartTempTask(void const * argument);
void PIDtimerCallback(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */
       
  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* Create the timer(s) */
  /* definition and creation of PIDtimer */
  osTimerDef(PIDtimer, PIDtimerCallback);
  PIDtimerHandle = osTimerCreate(osTimer(PIDtimer), osTimerPeriodic, NULL);

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  xTimerChangePeriod(PIDtimerHandle, 3000 / portTICK_PERIOD_MS, 100 );
  /* USER CODE END RTOS_TIMERS */

  /* Create the thread(s) */
  /* definition and creation of motorControlTas */
  osThreadDef(motorControlTas, StartMotorControlTask, osPriorityNormal, 0, 128);
  motorControlTasHandle = osThreadCreate(osThread(motorControlTas), NULL);

  /* definition and creation of PIDvalveTask */
  osThreadDef(PIDvalveTask, StartPIDvalveTask, osPriorityNormal, 0, 128);
  PIDvalveTaskHandle = osThreadCreate(osThread(PIDvalveTask), NULL);

  /* definition and creation of GUITask */
  osThreadDef(GUITask, StartGUITask, osPriorityNormal, 0, 128);
  GUITaskHandle = osThreadCreate(osThread(GUITask), NULL);

  /* definition and creation of manageTask */
  osThreadDef(manageTask, StartManageTask, osPriorityNormal, 0, 128);
  manageTaskHandle = osThreadCreate(osThread(manageTask), NULL);

  /* definition and creation of tempTask */
  osThreadDef(tempTask, StartTempTask, osPriorityNormal, 0, 128);
  tempTaskHandle = osThreadCreate(osThread(tempTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */
}

/* USER CODE BEGIN Header_StartMotorControlTask */
/**
  * @brief  Function implementing the motorControlTas thread.
  * @param  argument: Not used 
  * @retval None
  */
/* USER CODE END Header_StartMotorControlTask */
void StartMotorControlTask(void const * argument)
{

  /* USER CODE BEGIN StartMotorControlTask */
	init_valve();
	init_stepstick();
	base_valve();
	printf("based\r\n");

	osDelay(1000);


  /* Infinite loop */
		for(;;)
  	  {

			maintain_opening();

			if(valve.enable){
			rotate(valve.setStep);
			}

			osDelay(1);
	}
  /* USER CODE END StartMotorControlTask */
}

/* USER CODE BEGIN Header_StartPIDvalveTask */
/**
* @brief Function implementing the PIDvalveTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartPIDvalveTask */
void StartPIDvalveTask(void const * argument)
{
  /* USER CODE BEGIN StartPIDvalveTask */
  /* Infinite loop */
  for(;;)
  {
	  control_alghoritm();

    osDelay(1);
  }
  /* USER CODE END StartPIDvalveTask */
}

/* USER CODE BEGIN Header_StartGUITask */
/**
* @brief Function implementing the GUITask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartGUITask */
void StartGUITask(void const * argument)
{
  /* USER CODE BEGIN StartGUITask */

	  button_init(&inc);
	  button_init(&dec);
	  button_init(&enter);
	  button_init(&back);

	  ssd1331_init();
	  ssd1331_clear_screen(BLACK);
	  ssd1331_display_string(0, 0, " EEV DRIVER", FONT_1608, GREEN);
	  ssd1331_display_string(0, 16, "B.Michalak", FONT_1608, WHITE);
	  ssd1331_display_string(0, 32, "226392", FONT_1608, WHITE);
	  ssd1331_display_string(0, 52, "            v0.1", FONT_1206, WHITE);
	  osDelay(2000);

	  register_execute_node_fun_callback( execute_node );



  /* Infinite loop */
  for(;;)
  {
	  button_check_state(&inc,HAL_GPIO_ReadPin(INC_GPIO_Port,INC_Pin));
	  button_check_state(&dec,HAL_GPIO_ReadPin(DEC_GPIO_Port,DEC_Pin));
	  button_check_state(&enter,HAL_GPIO_ReadPin(ENTER_GPIO_Port,ENTER_Pin));
	  button_check_state(&back,HAL_GPIO_ReadPin(BACK_GPIO_Port,BACK_Pin));

	  settings();

	  check_events();

//	  if(!is_menu_on()){
//		  ssd1331_display_string(0, 0, "EEV v0.1", FONT_1608, GREEN);
//	  }

	  osDelay(1);

  }
  /* USER CODE END StartGUITask */
}

/* USER CODE BEGIN Header_StartManageTask */
/**
* @brief Function implementing the manageTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartManageTask */
void StartManageTask(void const * argument)
{
  /* USER CODE BEGIN StartManageTask */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END StartManageTask */
}

/* USER CODE BEGIN Header_StartTempTask */
/**
* @brief Function implementing the tempTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTempTask */
void StartTempTask(void const * argument)
{
  /* USER CODE BEGIN StartTempTask */
	HAL_ADC_Start_DMA(&hadc1, buf, 5);
  /* Infinite loop */
  for(;;)
  {
	  average();
	  calculate_value();

    osDelay(1);
  }
  /* USER CODE END StartTempTask */
}

/* PIDtimerCallback function */
void PIDtimerCallback(void const * argument)
{
  /* USER CODE BEGIN PIDtimerCallback */

	mdv.PID_loop = 1;
	mdv.manual++;
	oled_refresh = 1;
  
  /* USER CODE END PIDtimerCallback */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */
     
/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
