/*
 * valve.c
 *
 *  Created on: 31.08.2018
 *      Author: michalak
 */


#include "valve.h"



void base_valve(){
	enable(1);
	uint8_t initValueFlag = 0;
	while(!mdv.valveInit){
		if(!valve.enable){
			valve.actualStep = 0;
			mdv.actual_opening =0;
			valve.enable = 1;
			while(!initValueFlag){
				rotate(mdv.initial_value/100*valve.fullSteps);
				if(!valve.enable){
					initValueFlag = 1;
					mdv.valveInit = 1;
				}
			}
		}

	}
}

void init_valve(){
		mdv.initial_value = 60;		//initial open of EEV [0-100%]
		mdv.actual_opening = 0;		//actual open of EEV [0-100%]
		mdv.set_opening = 0;		//set opening of EEV [0-100%]
		mdv.eev_enable = 1;			//turn's on/off valve
		mdv.manual = 0;				//valve is in auto mode or manual
		mdv.valveInit = 0;			////info whether valve was based before rotate
}
