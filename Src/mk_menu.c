/*
 * mk_menu.c
 *
 *  Created on: 2018-01-30 
 *      Author: Miros�aw Karda�
 *         web: www.atnel.pl
 *   generator: MkMenuGen.EXE
 *   Copyright: ATNEL Miros�aw Karda�
 *   Nieautoryzowane kopiowanie, sprzeda�, udost�pnianie 
 *   kodu �r�owego bez zgody autora jest zabronione.
 *   Naruszeniem praw autorskich jest r�wnie� publikowanie 
 *   kodu �r�d�owego w internecie na forach, blogach itp.
 *
 */
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/wdt.h>
#include <string.h>
#include <stdio.h>


#include "mk_menu.h"

#include "../MK_MENU_USER_FILES/mk_menu_display.h"
#include "menu_items.h"


static uint8_t menu_on;
static uint8_t * timepout_ptr;

const static TNODE * actual_node_ptr;

// funkcja do rejestracji zmiennej, kt�ra odpowiada za timeout MENU, b�dzie wci��
// automatycznie zerowana podczas obs�ugi MENU
void register_timeout_counter_ptr( uint8_t * tptr ) {
	timepout_ptr = tptr;
}

// wska�nik do funkcji callback wykonuj�cej wybran� pozycj� MENU
static void (*execute_node_fun_callback)( uint8_t fun_id, uint8_t prop_id, uint8_t y );

// funkcja do rejestracji funkcji callbacka wykonuj�cego wybran� pozycj� MENU
void register_execute_node_fun_callback(void (*callback)(uint8_t fun_id, uint8_t prop_id, uint8_t y)) {
	execute_node_fun_callback = callback;
}

// wska�nik do funkcji callback wy�wietlaj�cy bie��c� pozycj� MENU - NODE
static void (*menu_node_callback)( const char * node_str );

// funkcja do rejestracji funkcji callbacka wy�wietlaj�cego bie��c� pozycj� MENU - NODE
void register_menu_node_callback(void (*callback)(const char * node_str)) {
	menu_node_callback = callback;
}

// wska�nik do funkcji callback wy�wietlaj�cy nazw� ga��zi i poprzedniej pozycju
static void (*branch_callback)( const char * branch_str, TNODE * active_node, TNODE * first_node );

// funkcja do rejestracji funkcji callbacka wy�wietlaj�cego nazw� ga��zi i poprzedniej pozycju
void register_menu_branch_callback(void (*callback)(const char * branch_str, TNODE * active_node, TNODE * first_node)) {
	branch_callback = callback;
}

// wska�nik do funkcji callback resetuj�cy pozycj� kontrolera obs�ugi MENU
static void (*reset_controller_pos_callback)( void );

// funkcja do rejestracji funkcji callbacka resetuj�cego pozycj� kontrolera obs�ugi MENU
void register_reset_controller_pos_callback( void (*callback)(void) ) {
	reset_controller_pos_callback = callback;
}


TNODE * get_first_node( void ) {
	return (TNODE *)pgm_read_word( &first_node_ptr );
}

TNODE * get_current_node( void ) {
	return (TNODE *)actual_node_ptr;
}

char * get_current_node_title( void ) {
	return (char*)pgm_read_word( &actual_node_ptr->node_title );
}

char * get_node_title( TNODE * ptr ) {
	return (char*)pgm_read_word( &ptr->node_title );
}

TNODE * get_next_node( TNODE * ptr ) {
	TNODE * res = (TNODE *)pgm_read_word( &ptr->node_next );
	return res;
}

TNODE * get_prev_node( TNODE * ptr ) {
	TNODE * res = (TNODE *)pgm_read_word( &ptr->node_prev );
	return res;
}

void set_current_node( TNODE * node_ptr ) {
	actual_node_ptr = node_ptr;
}

int8_t get_node_property_idx( TNODE * ptr ) {
	return pgm_read_byte( &ptr->property );
}

int8_t get_actual_property_idx( void ) {
	return pgm_read_byte( &actual_node_ptr->property );
}


uint8_t is_menu_on( void ) {
	return menu_on;
}

#if MULTILINE_MENU == 1
static void prepare_menu_branch( const char * branch_node_str, TNODE * active_node, TNODE * first_node ) {
	screen_cursor = 0;
	uint8_t idx = 0;
	while( first_node ) {
		if( first_node == active_node ) nodes_cursor = idx;
		nodes_list[idx] = first_node;
		first_node = get_next_node( first_node );
		nodes_count = idx;
		idx++;
	}
}


static void display_nodes( char * menu_string ) {
	static char * astr = NULL;
	static int8_t window_min, window_max;
	uint8_t can_display=0;
	if( menu_string ) {
		astr = menu_string;
		window_min = 0;
		if( nodes_count < MENU_LINES_COUNT ) window_max = nodes_count+1; else window_max = MENU_LINES_COUNT;
	}
	if( nodes_cursor > window_max-1 ) {
		window_min += nodes_cursor - window_max + 1;
		window_max += nodes_cursor - window_max + 1;
		can_display = 1;
	}
	else if( nodes_cursor < window_min ) {
		window_min -= window_min - nodes_cursor;
		window_max -= window_min - nodes_cursor + 1;
		can_display = 1;
	}
	else if( !can_display && !menu_string ) return;
	on_display_cls();
	on_display_menu_header( astr );
	uint8_t idx=0;
	for( uint8_t i=window_min; i<window_max; i++ ) {
		TNODE * ptr = nodes_list[i];

		on_display_menu_item( get_y_pos(idx), get_node_title(ptr) );

		int8_t prop_idx = get_node_property_idx(ptr);
		if( prop_idx >= 0 ) {

			on_display_menu_item_property( get_y_pos(idx), prop_idx );

		}
		if( menu_string && get_current_node() == ptr ) {

			on_display_item_cursor( get_y_pos(idx) );
			screen_cursor = idx;
			nodes_cursor = i;
		}
		idx++;
	}
	on_display_refresh();
}


static void decrement_menu_cursor( void ) {
	if( !menu_on ) return;
	if( timepout_ptr ) *timepout_ptr = 0;
	on_clear_item_cursor();
	if( nodes_cursor ) {
		nodes_cursor--;
		display_nodes(NULL);
		if( screen_cursor ) screen_cursor--;
	}
	on_display_item_cursor( get_y_pos(screen_cursor) );
	on_display_refresh();
}

static void increment_menu_cursor( void ) {
	if( !menu_on ) return;
	if( timepout_ptr ) *timepout_ptr = 0;
	on_clear_item_cursor();
	if( nodes_cursor < nodes_count ) {
		nodes_cursor++;
		display_nodes(NULL);
		if( screen_cursor < MENU_LINES_COUNT-1 ) {
			screen_cursor++;
		}
	}
	on_display_item_cursor( get_y_pos(screen_cursor) );
	on_display_refresh();
}


#endif


/* ***************************************************
 * mk_menu() - obs�uga MENU
 *
 * funkcj� mo�na obs�ugiwa� DOWOLNYM KONTROLEREM np:
 * przyciski, enkoder, pilot IR i inne
 *
 * (DO OBS�UGI wymagane s� trzy sygna�y/zmienne/przyciski)
 *
 * ARGUMENTY:
 * down_click_up = -1 kierunek lewo/g�ra
 * down_click_up =  1 kierunek prawo/d�
 * down_click_up =  0 klikni�cie / uruchomienie
 * down_click_up =  2 natychmiastowe opuszczenie MENU z dowolnego poziomu
 *
 * FLAGA GLOBALNA STANU MENU: menu_on
 * 0 - MENU niewidoczne / nie dzia�a
 * 1 - obs�uga MENU
 *
 */
static void mk_menu( int8_t down_click_up ) {
	if( timepout_ptr ) *timepout_ptr = 0;
#if MULTILINE_MENU == 1
	if( menu_on && down_click_up == 0 ) set_current_node( nodes_list[nodes_cursor] );
#endif
	uint8_t click = down_click_up == 0;
	if( 2 == down_click_up ) { 
		menu_on = 0;
		on_display_cls();
		on_display_refresh();
		return;
	}
	if( !menu_on && click ) {
		actual_node_ptr = get_first_node();
		TNODE * nptr = (TNODE*)actual_node_ptr;
		#if MULTILINE_MENU == 1
			prepare_menu_branch( NULL, nptr, (TNODE *)actual_node_ptr );
			display_nodes( (char*)main_menu_string );
		#endif
		if( branch_callback ) {
			branch_callback( NULL, nptr, (TNODE *)actual_node_ptr );
		}
		menu_on = 1;
		return;
	}
	if( menu_on ) {
		const TNODE * tmp = actual_node_ptr;
		if( 1 == down_click_up ) {
			tmp = (TNODE *)pgm_read_word( &actual_node_ptr->node_next );
		} else if( -1 == down_click_up ) {
			tmp = (TNODE *)pgm_read_word( &actual_node_ptr->node_prev );
		} else {
			if( click ) {
				int8_t function = pgm_read_word( &actual_node_ptr->function );
				TNODE * branch_next_ptr = (TNODE *)pgm_read_word( &actual_node_ptr->branch_next );
				TNODE * branch_prev_ptr = (TNODE *)pgm_read_word( &actual_node_ptr->branch_prev );
				TNODE * next_node = (TNODE *)pgm_read_word( &actual_node_ptr->node_next);
				int8_t actual_fun_id = pgm_read_word( &actual_node_ptr->function );
				int8_t actual_prop_id = pgm_read_word( &actual_node_ptr->property );
				if( branch_next_ptr ) {	
					char * bptr = (char*)pgm_read_word( &actual_node_ptr->node_title );
					TNODE * nptr = (TNODE*)branch_next_ptr;
					actual_node_ptr = branch_next_ptr;
					#if MULTILINE_MENU == 1
						prepare_menu_branch( bptr, nptr, (TNODE *)actual_node_ptr );
						display_nodes( bptr );
					#endif
					if( branch_callback ) {
						branch_callback( bptr, nptr, (TNODE *)actual_node_ptr );
					}
					return;
				}
				else if( function >= 0 && execute_node_fun_callback ) {
					TNODE * ptr = (TNODE *)actual_node_ptr;
					TNODE * aptr;
					while( ptr ) {
						aptr = (TNODE *)pgm_read_word( &ptr->node_next);
						if( !aptr ) break;
						ptr = aptr;
					}
					TNODE * prev_branch_prev_ptr = (TNODE *)pgm_read_word( &ptr->branch_prev );
					char * bptr = NULL;
					if( prev_branch_prev_ptr ) {
						bptr = (char*)pgm_read_word( &prev_branch_prev_ptr->node_title );
					}
					TNODE * nptr = (TNODE*)actual_node_ptr;
					ptr = (TNODE *)actual_node_ptr;
					while( ptr ) {
						aptr = (TNODE *)pgm_read_word( &ptr->node_prev);
						if( !aptr ) break;
						ptr = aptr;
					}
					execute_node_fun_callback( actual_fun_id, actual_prop_id, get_y_pos(screen_cursor) );
					#if MULTILINE_MENU == 1
						prepare_menu_branch( bptr, nptr, (TNODE *)ptr );
						if( bptr == NULL) display_nodes( (char*)main_menu_string );
						else display_nodes( bptr );
					#endif
					if( branch_callback ) {
						branch_callback( bptr, nptr, (TNODE *)ptr );
					}

					if( reset_controller_pos_callback ) reset_controller_pos_callback();
					return;
				}
				else if( !next_node ) {
					if( !branch_prev_ptr ) {
						menu_on = 0;
						on_display_cls();
						on_display_refresh();
						return;
					}
					else {
						TNODE * prev_branch_prev_ptr = (TNODE *)pgm_read_word( &branch_prev_ptr->branch_prev );
						char * bptr = NULL;
						if( prev_branch_prev_ptr ) {
							bptr = (char *)pgm_read_word( &prev_branch_prev_ptr->node_title );
						}
						actual_node_ptr = branch_prev_ptr;
						TNODE * nptr = (TNODE *)actual_node_ptr;
						TNODE * ptr = (TNODE *)actual_node_ptr;
						TNODE * aptr;
						while( ptr ) {
							aptr = (TNODE *)pgm_read_word( &ptr->node_prev);
							if( !aptr ) break;
							ptr = aptr;
						}
						#if MULTILINE_MENU == 1
							prepare_menu_branch( bptr, nptr, (TNODE *)ptr );
							if( bptr == NULL) display_nodes( (char*)main_menu_string );
							else display_nodes( bptr );
						#endif
						if( branch_callback ) {
							branch_callback( bptr, nptr, (TNODE *)ptr );
						}
						return;
					}
				}
			}
		}
		if( menu_on ) {
			if( tmp ) actual_node_ptr = tmp;
			if( menu_node_callback ) menu_node_callback( (char*)pgm_read_word( &actual_node_ptr->node_title ) );
		}
	}
}



void mk_menu_inc( void ) {
	#if MULTILINE_MENU == 1
		decrement_menu_cursor();
	#else
		mk_menu( -1 );
	#endif
}

void mk_menu_dec( void ) {
	#if MULTILINE_MENU == 1
	increment_menu_cursor();
	#else
		mk_menu( 1 );
	#endif
}

void mk_menu_click( void ) {
	mk_menu( 0 );
}

void mk_menu_toggle( void ) {
	if( !is_menu_on() ) mk_menu( 0 );
	else mk_menu( 2 );
}

void mk_menu_hide( void ) {
	mk_menu( 2 );
}


