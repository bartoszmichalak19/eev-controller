/*
 * mk_menu_exe.c
 *
 *  Created on: 2018-01-30 13:21:30
 *      Author: Miros�aw Karda�
 *         web: www.atnel.pl
 *   generator: MkMenuGen.EXE
 *   Copyright: ATNEL Miros�aw Karda�
 *   Nieautorywowane kopiowanie, sprzeda�,
 *   udost�pnianie kodu �r�owego zabronione.
 *   Zabronione jest r�wnie� publikowanie kodu
 *   �r�d�owego w internecie na forach, blogach itp.
 *
 */
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/wdt.h>
#include <string.h>
#include <stdio.h>

// #include " your display library "



#include "../MK_MENU_LIB/mk_menu.h"
#include "../MK_MENU_LIB/menu_items.h"



#include "mk_menu_display.h"
#include "mk_menu_exe.h"





void execute_node( uint8_t fun_id, uint8_t prop_id, uint8_t y ) {


	/* EXAMPLE for "Test data2" Menu */

// aby odkomentowana poni�ej sekcja dzia�a�a w przyk�adzie z poradnika wideo
// nale�y pami�ta� aby wcze�niej odkomentowa� r�wnie� sekcj� ustawie� dla tablicy
// Properties w pliku mk_menu_display.c

//	switch( fun_id ) {
//		case /* func 00 */ id_pin_nr_0_FP: {
//			if( ++menu_property[ prop_id ].byte > 7 ) menu_property[ prop_id ].byte = 0;
//			break;
//		}
//		case /* func 01 */ id_save_1_F:  break;
//		case /* func 02 */ id_ntp_sync_2_FP: menu_property[ prop_id ].byte ^= 1; break;
//		case /* func 03 */ id_pc_sync_3_FP: menu_property[ prop_id ].byte ^= 1; break;
//		case /* func 04 */ id_save_4_F:  break;
//		case /* func 05 */ id_date_5_FP: menu_property[ prop_id ].byte ^= 1; break;
//		case /* func 06 */ id_save_6_F:  break;
//		case /* func 07 */ id_enabled_7_FP: menu_property[ prop_id ].byte ^= 1; break;
//		case /* func 08 */ id_reg_adr_8_FP: {
//			if( ++(menu_property[ prop_id ].int16) > 8 ) menu_property[ prop_id ].int16 = -1;
//			break;
//		}
//		case /* func 09 */ id_save_9_F:  break;
//
////------------------------------------------- zmiana adres�w IP -- przyk�ad w OLED sample ----
//		case /* func 10 */ id_ip_10_FP:  break;
//		case /* func 11 */ id_mask_11_FP:  break;
//		case /* func 12 */ id_gate_12_FP:  break;
////------------------------------------------- zmiana adres�w IP end --------------------------
//
//		case /* func 13 */ id_host_13_FP:  break;
//		case /* func 14 */ id_start_14_F:  break;
//		case /* func 15 */ id_enabled_15_FP: menu_property[ prop_id ].byte ^= 1; break;
//		case /* func 16 */ id_pin_16_FP: {
//			if( ++menu_property[ prop_id ].byte > 7 ) menu_property[ prop_id ].byte = 0;
//			break;
//		}
//		case /* func 17 */ id_on_off_17_FP: menu_property[ prop_id ].byte ^= 1; break;
//		case /* func 18 */ id_night_mode_18_FP: menu_property[ prop_id ].byte ^= 1; break;
//		case /* func 19 */ id_save_19_F:  break;
//		case /* func 20 */ id_volume_20_FP: {
//			if( ++menu_property[ prop_id ].int16 > 10 ) menu_property[ prop_id ].int16 = 0;
//			break;
//		}
//		case /* func 21 */ id_save_21_F:  break;
//		case /* func 22 */ id_rows_22_FP: {
//			if( ++menu_property[ prop_id ].byte > 4 ) menu_property[ prop_id ].byte = 1;
//			break;
//		}
//		case /* func 23 */ id_columns_23_FP: {
//			if( menu_property[ prop_id ].byte == 16 ) menu_property[ prop_id ].byte = 20;
//			else if( menu_property[ prop_id ].byte == 20 ) menu_property[ prop_id ].byte = 8;
//			else if( menu_property[ prop_id ].byte == 8 ) menu_property[ prop_id ].byte = 16;
//			break;
//		}
//		case /* func 24 */ id_enabled_24_FP: menu_property[ prop_id ].byte ^= 1; break;
//		case /* func 25 */ id_save_25_F:  break;
//		case /* func 26 */ id_enabled_26_FP: menu_property[ prop_id ].byte ^= 1; break;
//		case /* func 27 */ id_bits_27_FP: {
//			if( ++menu_property[ prop_id ].byte > 9 ) menu_property[ prop_id ].byte = 5;
//			break;
//		}
//		case /* func 28 */ id_save_28_F:  break;
//	};




}


