/*
 * stepper.c
 *
 *  Created on: 16.08.2018
 *      Author: michalak
 */

#include "stepper.h"

stepper valve;

void rotate(int steps){

	//jesli kroki ujemne to odkrecaj, jesli dodatnie zakrecaj
	if(!valve.rotateInit){

		valve.rotateInit = 1;
		valve.savedStep = valve.actualStep;

			if(steps>0) set_direction(1);
			else set_direction(-1);
	}

	while(valve.actualStep == (valve.savedStep+steps)){
		take_step();
	}
	enable(0);

//	if(valve.actualStep <= valve.savedStep){
//		take_step();
//	}
//	else{
//
//	}
}

void take_step(){

	//tu ma się robic krok co okreslony czas wyliczany z zadanej czestotliwosci
	HAL_GPIO_WritePin(STEP_GPIO_Port,STEP_Pin,1);
	osDelay(1000/valve.frequency);
	valve.actualStep += valve.direction; //aktualizuje aktualny krok w zaleznosci od kierunku obrotu


}


void enable(uint8_t on){
	if(on){//ustaw pin na wlaczony
		valve.enable = 1;
	}
	else{
		//ustaw pin na wylaczony
		valve.enable = 0;
	}
}

void set_direction(int8_t dir){
	//zakrecanie
	if(dir==1){
		//ustaw pin na wlaczony
			valve.direction = 1;
		}
	//odkrecanie
		if(dir==-1){
			//ustaw pin na wylaczony
			valve.direction = -1;
		}
}


