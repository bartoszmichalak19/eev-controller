/*
 * GUI.h
 *
 *  Created on: 13.11.2018
 *      Author: DELL
 */

#ifndef GUI_H_
#define GUI_H_

#include "stm32f4xx_hal.h"
#include "mk_menu.h"
#include "SSD1331.h"
#include "valve.h"
#include "sensors.h"

#include <stdint.h>


#define CONFID_THRESHOLD 10

typedef struct Button
{
    uint8_t isOn;
    uint8_t eventRising;
	uint16_t onConfidenceLVL;
	uint16_t offConfidenceLVL;
	uint16_t confidenceTreshold;

} Button;

Button inc,dec,enter,back;

extern uint8_t menuUse;         		//if button is used in menu =1
extern uint8_t flag_set;
extern uint8_t oled_refresh;
extern float tmp2;
extern float tmp3[4];
extern char buffer[16];


void button_init(Button *sharp);
void button_check_state(Button *sharp, GPIO_PinState state);
void check_events();
void settings();

#endif /* GUI_H_ */
