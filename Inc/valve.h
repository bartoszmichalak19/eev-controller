/*
 * valve.h
 *
 *  Created on: 31.08.2018
 *      Author: michalak
 */

#ifndef VALVE_H_
#define VALVE_H_

#include "main.h"
#include "stepper.h"

typedef struct EEV{

	uint8_t PID_loop;			//info from timer after period
	uint8_t initial_value;		//initial open of EEV [0-100%]
	float actual_opening;		//actual opening of EEV [0-100%]
	float set_opening;		//set opening of EEV [0-100%]
	uint8_t eev_enable;			//turn's on/off valve
	uint8_t manual;				//valve is in auto mode or manual
	uint8_t valveInit;			//info whether valve was based before rotate
	float actual_deltaT;				//difference between tempIN and tempOUT of evaportor, main parameter
	float set_deltaT;
}EEV;

typedef struct Ctrl{ 			//Ctrl - Controller

	float error,prev_error; 	//difference between set_value and current_value
	//dodac limit sumatora w calkowaniu
	float Kp,Ki,Kd;
	float prop,inte,deri;


}Ctrl;

EEV mdv;
Ctrl PID;						//PID Controller

void init_valve();

void control_alghoritm();		//PID regulator implementation, calculates valve opening

void maintain_opening(); 		//keeps set valve opening

void base_valve();

#endif /* VALVE_H_ */
