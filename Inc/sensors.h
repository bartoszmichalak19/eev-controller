/*
 * sensors.h
 *
 *  Created on: 06.09.2018
 *      Author: michalak
 */

#ifndef SENSORS_H_
#define SENSORS_H_

#define adc_resolution 4095.0
#define supply_voltage 2.93
//#define v_transmission ((47+68)/68)

#define Rref 10000.0
#define Tref 298.0
#define beta 3950.0

#define samplesTemp 3000
#define samplesVolt 500

#include "main.h"
#include "adc.h"
#include "math.h"
#include "valve.h"

uint32_t buf[5];
uint16_t adcMsr[5];
uint8_t eoc;
uint8_t avg_done;
uint16_t actual_samples;


//typedef struct{
//
//	float tIn;		//tempIN evaporator
//	float tOut;		//tempOUT evaporator
//	float tInside;	//tempInside evaporator
//	float pressure; //pressure at eev
//	uint16_t pot;   //adc from potentiometer
//
//}sensors;


typedef struct {

	uint16_t samples;
	uint32_t sum;
	uint16_t result_adc;
	uint16_t helper;
	float voltage;
	float value;


}sensor;

sensor T1,T2,T3,P1,POT;


void average();

void calculate_value();


#endif /* SENSORS_H_ */
