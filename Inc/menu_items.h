/*
 * mk_items.h
 *
 *  Created on: 13.11.2018 22:52:54
 *      Author: Miros�aw Karda�
 *         web: www.atnel.pl
 *   generator: MkMenuGen.EXE
 *   Copyright: ATNEL Miros�aw Karda�
 *   Nieautoryzowane kopiowanie, sprzeda�, udost�pnianie
 *   kodu �r�owego bez zgody autora jest zabronione.
 *   Naruszeniem praw autorskich jest r�wnie� publikowanie
 *   kodu �r�d�owego w internecie na forach, blogach itp.
 *
 */
#include <inttypes.h>

#include "mk_menu.h"

#ifndef MK_MENU_LIB_MENU_ITEMS_H_
#define MK_MENU_LIB_MENU_ITEMS_H_

//------------------ ustawienia parametr�w wy�wietlania ---------
#define MENU_LINES_COUNT      4
#define FIRST_LINE_POS        16
#define LINE_HEIGHT           12
//-----------------------------------------------

inline uint8_t get_y_pos(uint8_t y) {
	return FIRST_LINE_POS + (y * LINE_HEIGHT);
}

extern const char main_menu_string[];

#define MULTILINE_MENU    1

extern const TNODE * const first_node_ptr;

#define MENU_PROPERTY_COUNT    0

// maximum nodes in one menu level
#define MAX_LEVEL_NODES    4

extern int8_t screen_cursor;
extern int8_t nodes_cursor;
extern TNODE * nodes_list[];
extern uint8_t nodes_count;

enum {
	id_LQAP10a_0_F = 0,
	id_SOAT11a_1_F = 1,
	id_KFSJ13a_2_F = 2,
	id_IKMJ14a_3_F = 3,
	id_GGLB15a_4_F = 4,
	id_MURN18a_5_F = 5,
};

/* copy and uncomment it into your execute_node_fun_callback function */
//switch( fun_id ) {
//    case /* func 00 */ id_LQAP10a_0_F:  break;
//    case /* func 01 */ id_SOAT11a_1_F:  break;
//    case /* func 02 */ id_KFSJ13a_2_F:  break;
//    case /* func 03 */ id_IKMJ14a_3_F:  break;
//    case /* func 04 */ id_GGLB15a_4_F:  break;
//    case /* func 05 */ id_MURN18a_5_F:  break;
//};

extern const TNODE NGSM0Fa_0;
extern const TNODE LQAP10a_1;
extern const TNODE SOAT11a_2;
extern const TNODE JGIE12a_3;
extern const TNODE KFSJ13a_4;
extern const TNODE IKMJ14a_5;
extern const TNODE GGLB15a_6;
extern const TNODE back_7;
extern const TNODE back_8;
extern const TNODE MURN18a_9;
extern const TNODE exit_10;

#endif /* MK_MENU_LIB_MENU_ITEMS_H_ */
