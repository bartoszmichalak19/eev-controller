/*
 * mk_menu_exe.h
 *
 *  Created on: 2018-01-30 13:21:30
 *      Author: Miros�aw Karda�
 *         web: www.atnel.pl
 *   generator: MkMenuGen.EXE
 *   Copyright: ATNEL Miros�aw Karda�
 *   Nieautorywowane kopiowanie, sprzeda�,
 *   udost�pnianie kodu �r�owego zabronione.
 *   Zabronione jest r�wnie� publikowanie kodu
 *   �r�d�owego w internecie na forach, blogach itp.
 *
 */
#include <inttypes.h>

#include "mk_menu.h"
#include "GUI.h"

#ifndef MK_MENU_EXE_H_
#define MK_MENU_EXE_H_




void execute_node( uint8_t fun_id, uint8_t prop_id, uint8_t y );



#endif /* MK_MENU_EXE_H_ */
