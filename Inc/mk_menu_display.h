/*
 * mk_menu_display.h
 *
 *  Created on: 2018-01-30 
 *      Author: Miros�aw Karda�
 *         web: www.atnel.pl
 *   generator: MkMenuGen.EXE
 *   Copyright: ATNEL Miros�aw Karda�
 *   Nieautoryzowane kopiowanie, sprzeda�, udost�pnianie 
 *   kodu �r�owego bez zgody autora jest zabronione.
 *   Naruszeniem praw autorskich jest r�wnie� publikowanie 
 *   kodu �r�d�owego w internecie na forach, blogach itp.
 *
 */
#include <inttypes.h>
#include "pgmspace.h"

#include "mk_menu.h"
#include "menu_items.h"

#ifndef MK_MENU_DISPLAY_H_
#define MK_MENU_DISPLAY_H_


// it's only specific example for OLED Library
// #define MAGNIFI			1



enum { _bool, _byte, _int, _str, _float };

typedef struct {
	uint8_t type;
	union {
		uint8_t byte;
		int int16;
		int32_t int32;
		float flt;
	};
	union {
		uint8_t def_byte;
		int def_int16;
		uint32_t def_uint32;
		float def_flt;
	};
	char * string;
	char * def_str;
} TPROPERTY;


extern TPROPERTY menu_property[];


void on_display_cls( void );
void on_display_refresh( void );


#if MULTILINE_MENU == 1

void on_display_menu_header( const char * astr );
void on_display_menu_item( int y, const char * item_string );
void on_display_menu_item_property( int y, uint8_t prop_idx );
void on_display_item_cursor( int y );
void on_clear_item_cursor( void );

#endif


// callbacks
void display_menu_node( const char * node_str );
void display_menu_branch( const char * branch_node_str, TNODE * active_node, TNODE * first_node );


#endif /* MK_MENU_DISPLAY_H_ */
