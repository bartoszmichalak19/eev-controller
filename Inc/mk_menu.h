/*
 * mk_menu.h
 *
 *  Created on: 2018-01-30 
 *      Author: Miros�aw Karda�
 *         web: www.atnel.pl
 *   generator: MkMenuGen.EXE
 *   Copyright: ATNEL Miros�aw Karda�
 *   Nieautoryzowane kopiowanie, sprzeda�, udost�pnianie 
 *   kodu �r�owego bez zgody autora jest zabronione.
 *   Naruszeniem praw autorskich jest r�wnie� publikowanie 
 *   kodu �r�d�owego w internecie na forach, blogach itp.
 *
 */
#include <inttypes.h>
#include "SSD1331.h"



#ifndef MK_MENU_LIB_MK_MENU_H_
#define MK_MENU_LIB_MK_MENU_H_



typedef struct ANODE {
	const uint8_t node_id;
	const char * node_title;
	const int8_t function;
	const int8_t property;

	const struct ANODE * branch_next;
	const struct ANODE * branch_prev;

	const struct ANODE * node_next;
	const struct ANODE * node_prev;

} TNODE;



uint8_t is_menu_on( void );



void mk_menu_inc( void );
void mk_menu_dec( void );
void mk_menu_click( void );
void mk_menu_toggle( void );
void mk_menu_hide( void );


void register_menu_node_callback(void (*callback)(const char * node_str));
void register_menu_branch_callback(void (*callback)(const char * branch_str, TNODE * active_node, TNODE * first_node));
void register_execute_node_fun_callback(void (*callback)(uint8_t fun_id, uint8_t prop_id, uint8_t y));
void register_reset_controller_pos_callback( void (*callback)(void) );

void register_timeout_counter_ptr( uint8_t * tptr );



TNODE * get_first_node( void );

TNODE * get_current_node( void );
void set_current_node( TNODE * node_ptr );
char * get_current_node_title( void );
char * get_node_title( TNODE * ptr );
TNODE * get_next_node( TNODE * ptr );
TNODE * get_prev_node( TNODE * ptr );

int8_t get_node_property_idx( TNODE * ptr );
int8_t get_actual_property_idx( void );



#endif /* MK_MENU_LIB_MK_MENU_H_ */
