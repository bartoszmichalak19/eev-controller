/*
 * stepper.h
 *
 *  Created on: 16.08.2018
 *      Author: michalak
 */

#ifndef STEPPER_H_
#define STEPPER_H_

#include "main.h"
#include "valve.h"
#include "FreeRTOS.h"
#include "task.h"

typedef struct{

	float actualStep;         //actual step of motor
	int16_t savedStep;			 //needed to save previous step
	float totalSteps;		  //total steps to 100% open valve
	uint16_t initialStep;		  //starting point after initial function
	uint16_t frequency;			  //steps per second
	int8_t direction;			  //direction of rotation 1-opening 0-closing
	uint8_t enable;				  //turn's on/off valve
	uint8_t rotateInit;			  //info whether valve was based before rotate
	int16_t setStep;			  //actual setStep

}stepper;

stepper valve;

void rotate(int steps);

void init_stepstick();

void take_step();

void enable(uint8_t on);

void set_direction(int8_t dir);

#endif /* STEPPER_H_ */
